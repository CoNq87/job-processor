## Priority Job Processor

The solution contains 3 implementation of the JobProcessor.
I was focusing on the task and not on reliability therefore there is no persistence context in
this little application.
The current solution only contains a dummy empty run method for the Tasks (JobResult) which makes the thread sleep
a bit to make the application more realistic. 

#### SimplePriorityJobProcessor
This solution ensures that the HIGH prio issues will be process ASAP but
it does not guarantees protection against starvation. It does not contain any speciality.
This solution is based on the following article:  
https://www.baeldung.com/java-priority-job-schedule


#### MultiQueuePriorityJobProcessor
This solution handles the 3 level of priorities separately.
There are independent queues for the LOW,MEDIUM and HIGH priority jobs. 
It guarantees that the lower jobs will be also processed.
All the 3 priority level has it's own queue and those are configurable
(number of threads for a priority lewel)
This solution does not guarantees that the HIGH-est jobs will be processed first 
but if the HIGH priority task executor gets more threads it will process faster
then the lower queues. 

#### RebalancingMultiQueuePriorityJobProcessor
The idea is based on CPU scheduler algorithms.
It is an extension of the MultiQueuePriorityJobProcessor logic.
In short: It prioritize the jobs if starvation detected on a lower queue 
and the higher queues are not too busy.
Does the same as the MultiQueuePriorityJobProcessor but with one difference. If the lower level queues are overloaded
and the higher level queues are not than this solution (based on the configured threshold)
will ship the tasks to the higher level queues and therefore all the threads will work 
parallelly.

The simple logic is the following.
LOW queue processor can forward the task into the MEDIUM queue processor.

MEDIUM queue processor can forward the task into the HIGH queue processor.

The algorithm:
If the *rebalancingAfterSeconds* property is 4 seconds
then the LOW and MEDIUM queue processor if finds that a task is older than 4s
then it checks the throughput capacity of the next level queue and if it 
is satisfying then it will pass the Job to the next level queue. Otherwise it will process it.

The throughput capacity calculation: 
if(CURRENT-QUEUE-PRESSURE > NEXT-LEVEL-QUEUE-PRESSURE * 2) then push the task into the next level queue.
otherwise process it. 
The multiplier (*2) ensures that the higher level queue never should be overloaded cause of the lower level
queues. There has to be more reserve for high prio Jobs.

The pressure calculation: 
{ number of tasks on the queue} / { number of processor threads} 
In words it is "the number of messages / threads" on a queue.

#### How to test it
###### via Test classes
There are @Ignored tests which can be used to generate a load on the given processor.

###### via Rest (suggested way)
Configure the desired processor via the  job-processor/src/main/resources/application.yml
Set the following property (the others are fine):
job:
  processor:
    strategy: Simple #valid values are: Simple, MultiQueue, RebalancingMultiQueue

Start the application up. 
There are no dependencies. It is an in-memory implementation of the task.
Use the curl command which is given at the end of this README.



#### Further improvements
-Configure environment variables
-More meaningful tests
-Validations
-Advanced error handling
-Real Task implementation




#### Curl command 

curl -X POST http://localhost:8080/jobs/ -H'Content-Type:application/json' -H'Host:localhost:8080' -d'[{"priority":"LOW"},{"priority":"LOW"},{"priority":"LOW"},{"priority":"LOW"},{"priority":"LOW"},{"priority":"LOW"},{"priority":"LOW"},{"priority":"LOW"},{"priority":"LOW"},{"priority":"LOW"},{"priority":"LOW"},{"priority":"LOW"},{"priority":"MEDIUM"},{"priority":"LOW"},{"priority":"MEDIUM"},{"priority":"LOW"},{"priority":"HIGH"},{"priority":"LOW"}]'