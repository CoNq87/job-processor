package com.job.processor;

import com.job.model.Job;

public interface JobProcessor<T extends Job> {

    void scheduleJob(T job);
}
