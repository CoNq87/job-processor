package com.job.processor;


import com.job.model.JobResult;
import lombok.ToString;

import java.util.Comparator;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.PriorityBlockingQueue;

/**
 * A simple implementation of the JobProcessor which can lead to job starvation
 */
@ToString
public class SimplePriorityJobProcessor implements JobProcessor<JobResult> {

    private ExecutorService priorityJobPoolExecutor;
    private ExecutorService priorityJobScheduler
            = Executors.newSingleThreadExecutor();
    private PriorityBlockingQueue<JobResult> priorityQueue;
    private Integer poolSize;
    private Integer queueSize;
    private Comparator<JobResult> jobResultComparator = new JobResultComparator();

    public SimplePriorityJobProcessor(Integer poolSize, Integer queueSize) {
        this.poolSize = poolSize;
        this.queueSize = queueSize;
        priorityJobPoolExecutor = Executors.newFixedThreadPool(poolSize);
        priorityQueue = new PriorityBlockingQueue<JobResult>(
                queueSize,
                jobResultComparator
        );
        priorityJobScheduler.execute(() -> {
            while (true) {
                try {
                    priorityJobPoolExecutor.execute(priorityQueue.take());
                } catch (InterruptedException e) {
                    // exception needs special handling
                    break;
                }
            }
        });
    }

    @Override
    public void scheduleJob(JobResult job) {
        priorityQueue.add(job);
    }

    private static class JobResultComparator implements Comparator<JobResult> {
        @Override
        public int compare(JobResult o1, JobResult o2) {
            return -1 * o1.getPriority().compareTo(o2.getPriority());
        }
    }


}
