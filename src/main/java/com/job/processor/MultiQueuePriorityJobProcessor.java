package com.job.processor;

import com.job.model.Job;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.function.BiConsumer;

@ToString
@Slf4j
public class MultiQueuePriorityJobProcessor<T extends Job> implements JobProcessor<T> {
    @ToString.Exclude
    protected ExecutorService lowPriorityJobPoolExecutor;
    @ToString.Exclude
    protected ExecutorService mediumPriorityJobPoolExecutor;
    @ToString.Exclude
    protected ExecutorService highPriorityJobPoolExecutor;

    @ToString.Exclude
    protected ExecutorService priorityJobScheduler
            = Executors.newFixedThreadPool(3);

    @ToString.Exclude
    protected LinkedBlockingQueue<T> lowPriorityQueue;
    @ToString.Exclude
    protected LinkedBlockingQueue<T> mediumPriorityQueue;
    @ToString.Exclude
    protected LinkedBlockingQueue<T> highPriorityQueue;

    protected Integer lowPoolSize;
    protected Integer lowQueueSize;
    protected Integer mediumPoolSize;
    protected Integer mediumQueueSize;
    protected Integer highPoolSize;
    protected Integer highQueueSize;

    protected BiConsumer<ExecutorService, LinkedBlockingQueue<T>> simpleProcessor;



    public MultiQueuePriorityJobProcessor(Integer lowPoolSize, Integer lowQueueSize, Integer mediumPoolSize, Integer mediumQueueSize, Integer highPoolSize, Integer highQueueSize) {
        this.lowPoolSize = lowPoolSize;
        this.lowQueueSize = lowQueueSize;
        this.mediumPoolSize = mediumPoolSize;
        this.mediumQueueSize = mediumQueueSize;
        this.highPoolSize = highPoolSize;
        this.highQueueSize = highQueueSize;

        initThreadPoolExecutors(lowPoolSize, mediumPoolSize, highPoolSize);
        initQueues();
        initProcessors();
        startProcessors();

    }

    protected void initProcessors() {
        this.simpleProcessor = (executor, queue) -> {
            try {
                executor.execute(queue.take());
            } catch (InterruptedException e) {
                RuntimeException error = new RuntimeException("Interruption occurred in the processing thread", e);
                log.error(error.getMessage(), error);
            }
        };
    }

    private void initThreadPoolExecutors(Integer lowPoolSize, Integer mediumPoolSize, Integer highPoolSize) {
        lowPriorityJobPoolExecutor = Executors.newFixedThreadPool(lowPoolSize);
        mediumPriorityJobPoolExecutor = Executors.newFixedThreadPool(mediumPoolSize);
        highPriorityJobPoolExecutor = Executors.newFixedThreadPool(highPoolSize);
    }

    protected void initQueues() {
        lowPriorityQueue = new LinkedBlockingQueue<T>(lowQueueSize);
        mediumPriorityQueue = new LinkedBlockingQueue<T>(mediumQueueSize);
        highPriorityQueue = new LinkedBlockingQueue<T>(highQueueSize);
    }

    protected void startProcessors() {

        startProcessing(highPriorityJobPoolExecutor, highPriorityQueue, simpleProcessor);
        startProcessing(mediumPriorityJobPoolExecutor, mediumPriorityQueue, simpleProcessor);
        startProcessing(lowPriorityJobPoolExecutor, lowPriorityQueue, simpleProcessor);
    }

    protected void startProcessing(ExecutorService priorityJobPoolExecutor, LinkedBlockingQueue<T> priorityQueue, BiConsumer<ExecutorService,LinkedBlockingQueue<T>> processor) {
        priorityJobScheduler.execute(() -> {
            while (true) {
                    processor.accept(priorityJobPoolExecutor,priorityQueue);
            }
        });
    }

    @Override
    public void scheduleJob(T job) {
        log.debug("Jobb added: {}" , job);
        switch (job.getPriority()) {
            case LOW:
                lowPriorityQueue.add(job);
                break;
            case MEDIUM:
                mediumPriorityQueue.add(job);
                break;
            case HIGH:
                highPriorityQueue.add(job);
                break;
            //Default: Normally this cannot happen, only if someone massed up the code
            default:
                log.error("Unknown job priority,the job will be ignored, job: {}", job);
        }

    }

}
