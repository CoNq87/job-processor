package com.job.processor;

import com.job.model.Job;
import com.job.model.Priority;
import com.job.model.WrappedWithContextJobResult;
import lombok.Getter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

import java.time.Instant;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.function.BiConsumer;

@ToString(callSuper = true)
@Slf4j
public class RebalancingMultiQueuePriorityJobProcessor extends MultiQueuePriorityJobProcessor<Job> {


    protected BiConsumer<ExecutorService, LinkedBlockingQueue<Job>> rebalancingProcessor;
    private Long rebalancingAfterSeconds;

    public RebalancingMultiQueuePriorityJobProcessor(Integer lowPoolSize, Integer lowQueueSize, Integer mediumPoolSize, Integer mediumQueueSize, Integer highPoolSize, Integer highQueueSize, Long rebalancingAfterSeconds) {
        super(lowPoolSize, lowQueueSize, mediumPoolSize, mediumQueueSize, highPoolSize, highQueueSize);
        this.rebalancingAfterSeconds = rebalancingAfterSeconds;
    }

    @Override
    protected void startProcessors() {
        startProcessing(highPriorityJobPoolExecutor, highPriorityQueue, simpleProcessor);
        startProcessing(mediumPriorityJobPoolExecutor, mediumPriorityQueue, rebalancingProcessor);
        startProcessing(lowPriorityJobPoolExecutor, lowPriorityQueue, rebalancingProcessor);
    }

    @Override
    protected void initProcessors() {
        this.simpleProcessor = (executor, queue) -> {

            WrappedWithContextJobResult wrappedJob = getWrappedWithContextJobResult(queue);
            Priority currentLevel = ((ExtendedLinkedBlockingQueue) queue).getLevel();
            log.info("Execution starting up on queue: {} for job: {}", currentLevel, wrappedJob);
            Future<?> submitedTask = executor.submit(wrappedJob);
            waitOnCompletion(submitedTask);

        };

        this.rebalancingProcessor = (executor, queue) -> {

            WrappedWithContextJobResult wrappedJob = getWrappedWithContextJobResult(queue);

            Priority currentLevel = ((ExtendedLinkedBlockingQueue) queue).getLevel();
            log.trace("---------------------");
            log.trace("wrappedJob.getReceivedAt()" + wrappedJob.getReceivedAt());
            log.trace("Instant.now().minusSeconds(rebalancingAfterSeconds)" + Instant.now().minusSeconds(rebalancingAfterSeconds));
            if (wrappedJob.getReceivedAt().isBefore(Instant.now().minusSeconds(rebalancingAfterSeconds))) {

                //If the job is old enough
                //and the one level higher priority queue has more resources then we pass it up to the next level
                //constant because LinkedBlockingQueue stores the number of elements
                if (currentLevel.equals(Priority.LOW)) {
                    if (isTheLowPrioMoreBusyThanTheMedium()) {
                        log.info("Prioritising up to MEDIUM queue the following job:{} ", wrappedJob);
                        mediumPriorityQueue.add(wrappedJob);
                        return;
                    }
                } else if (currentLevel.equals(Priority.MEDIUM)) {
                    if (isTheMediumPrioMoreBusyThanTheHigh()) {
                        log.info("Prioritising up to HIGH queue the following job:{} , up to HIGH queue", wrappedJob);
                        highPriorityQueue.add(wrappedJob);
                        return;
                    }
                }
            }
            log.info("Execution starting up on queue: {} for job: {}", currentLevel, wrappedJob);
            Future<?> submitedTask = executor.submit(wrappedJob);
            waitOnCompletion(submitedTask);


        };
    }

    private WrappedWithContextJobResult getWrappedWithContextJobResult(LinkedBlockingQueue<Job> queue) {
        WrappedWithContextJobResult wrappedJob = null;
        try {
            wrappedJob = (WrappedWithContextJobResult) queue.take();
        } catch (Exception e) {
            RuntimeException error = new RuntimeException("Exception occurred in the processing thread", e);
            log.error(error.getMessage(), error);
        }
        return wrappedJob;
    }

    private void waitOnCompletion(Future<?> submitedTask) {
        while (!submitedTask.isDone()) {
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                RuntimeException error = new RuntimeException("Exception occurred in the processing thread", e);
                log.error(error.getMessage(), error);
            }
        }
    }

    private boolean isTheLowPrioMoreBusyThanTheMedium() {
        return (lowPriorityQueue.size() / this.lowPoolSize) > ((mediumPriorityQueue.size() / this.mediumPoolSize) * 2);
    }

    private boolean isTheMediumPrioMoreBusyThanTheHigh() {
        return (mediumPriorityQueue.size() / this.mediumPoolSize) > ((highPriorityQueue.size() / this.highPoolSize) * 2);
    }

    @Override
    public void scheduleJob(Job job) {
        WrappedWithContextJobResult wrappedWithContextJobResult = new WrappedWithContextJobResult(job);
        super.scheduleJob(wrappedWithContextJobResult);
    }

    @Override
    protected void initQueues() {
        lowPriorityQueue = new ExtendedLinkedBlockingQueue<Job>(lowQueueSize, Priority.LOW);
        mediumPriorityQueue = new ExtendedLinkedBlockingQueue<Job>(mediumQueueSize, Priority.MEDIUM);
        highPriorityQueue = new ExtendedLinkedBlockingQueue<Job>(highQueueSize, Priority.HIGH);
    }

    @Getter
    private class ExtendedLinkedBlockingQueue<T> extends LinkedBlockingQueue<T> {
        Priority level;

        public ExtendedLinkedBlockingQueue(int capacity, Priority level) {
            super(capacity);
            this.level = level;
        }
    }
}
