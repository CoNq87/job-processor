package com.job.model;

import lombok.Data;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

import java.time.Instant;

/*
Using delegation to JobResult and adds startTime handling
The original Job should not be altered anyway
 */
@Slf4j
@ToString
@Data
public class WrappedWithContextJobResult implements Job {

    Job job;
    Instant receivedAt;

    public WrappedWithContextJobResult(Job job) {
        this.job = job;
        receivedAt = Instant.now();
    }

    @Override
    public Priority getPriority() {
        return this.job.getPriority();
    }

    @Override
    public void setPriority(Priority priority) {
        this.job.setPriority(priority);
    }

    @Override
    public void run() {
        this.job.run();
    }
}