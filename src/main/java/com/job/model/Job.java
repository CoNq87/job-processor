package com.job.model;

public interface Job extends Runnable {

    Priority getPriority();

    void setPriority(Priority priority);

    void run();
}
