package com.job.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Data
@AllArgsConstructor
@NoArgsConstructor
public class JobResult implements Job {
    private Priority priority;

    @Override
    public void run() {
        log.info("Executing Job {}", this);
        executeTheJob();

    }

    private void executeTheJob() {
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            throw new RuntimeException("Unexpected error on job execution, job: {}", e);
        }
        log.info("Executed Job {}", this);

    }

}
