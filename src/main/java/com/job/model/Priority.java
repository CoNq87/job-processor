package com.job.model;

public enum Priority {
    LOW,
    MEDIUM,
    HIGH
}