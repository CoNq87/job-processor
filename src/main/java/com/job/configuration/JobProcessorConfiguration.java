package com.job.configuration;

import com.job.model.JobResult;
import com.job.processor.JobProcessor;
import com.job.processor.MultiQueuePriorityJobProcessor;
import com.job.processor.RebalancingMultiQueuePriorityJobProcessor;
import com.job.processor.SimplePriorityJobProcessor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Slf4j
@Configuration
public class JobProcessorConfiguration {

    private JobProcessorProperties processorProperties;

    @Bean
    @ConditionalOnProperty(prefix = "job.processor", value = "strategy", havingValue = "Simple")
    public JobProcessor simplePriorityJobProcessor(JobProcessorProperties processorProperties) {
        this.processorProperties = processorProperties;
        SimplePriorityJobProcessor simplePriorityJobProcessor = new SimplePriorityJobProcessor(processorProperties.getProcessor().getSimple().getPoolSize(), processorProperties.getProcessor().getSimple().getQueueSize());
        log.debug("Configured SimplePriorityJobProcessor: {}", simplePriorityJobProcessor);
        return simplePriorityJobProcessor;
    }

    @Bean
    @ConditionalOnProperty(prefix = "job.processor", value = "strategy", havingValue = "MultiQueue")
    public JobProcessor multiQueuePriorityJobProcessor() {
        MultiQueuePriorityJobProcessor multiQueuePriorityJobProcessor = new MultiQueuePriorityJobProcessor<JobResult>(
                processorProperties.getProcessor().getMultiQueue().getLowPoolSize(),
                processorProperties.getProcessor().getMultiQueue().getLowQueueSize(),
                processorProperties.getProcessor().getMultiQueue().getMediumPoolSize(),
                processorProperties.getProcessor().getMultiQueue().getMediumQueueSize(),
                processorProperties.getProcessor().getMultiQueue().getHighPoolSize(),
                processorProperties.getProcessor().getMultiQueue().getHighQueueSize()

        );
        log.debug("Configured MultiQueuePriorityJobProcessor: {}", multiQueuePriorityJobProcessor);
        return multiQueuePriorityJobProcessor;
    }

    @Bean
    @ConditionalOnProperty(prefix = "job.processor", value = "strategy", havingValue = "RebalancingMultiQueue")
    public JobProcessor rebalancingMultiQueuePriorityJobProcessor() {
        RebalancingMultiQueuePriorityJobProcessor rebalancingMultiQueuePriorityJobProcessor = new RebalancingMultiQueuePriorityJobProcessor(
                processorProperties.getProcessor().getRebalancingMultiQueue().getLowPoolSize(),
                processorProperties.getProcessor().getRebalancingMultiQueue().getLowQueueSize(),
                processorProperties.getProcessor().getRebalancingMultiQueue().getMediumPoolSize(),
                processorProperties.getProcessor().getRebalancingMultiQueue().getMediumQueueSize(),
                processorProperties.getProcessor().getRebalancingMultiQueue().getHighPoolSize(),
                processorProperties.getProcessor().getRebalancingMultiQueue().getHighQueueSize(),
                processorProperties.getProcessor().getRebalancingMultiQueue().getRebalancingAfterSeconds()

        );
        log.debug("Configured RebalancingMultiQueuePriorityJobProcessor: {}", rebalancingMultiQueuePriorityJobProcessor);
        return rebalancingMultiQueuePriorityJobProcessor;
    }

}
