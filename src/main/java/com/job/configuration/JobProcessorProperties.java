package com.job.configuration;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
@Component
@ConfigurationProperties(prefix = "job")
public class JobProcessorProperties {

    private Processor processor;

    @Data
    public static class Processor{
        private String strategy;
        private Simple simple;
        private MultiQueue multiQueue;
        private RebalancingMultiQueue rebalancingMultiQueue;
    }

    @Data
    public static class Simple{
        private Integer poolSize;
        private Integer queueSize;
    }

    @Data
    public static class MultiQueue{
        private Integer lowPoolSize;
        private Integer lowQueueSize;
        private Integer mediumPoolSize;
        private Integer mediumQueueSize;
        private Integer highPoolSize;
        private Integer highQueueSize;
    }

    @Data
    public static class RebalancingMultiQueue{
        private Integer lowPoolSize;
        private Integer lowQueueSize;
        private Integer mediumPoolSize;
        private Integer mediumQueueSize;
        private Integer highPoolSize;
        private Integer highQueueSize;
        private Long rebalancingAfterSeconds;
    }



}
