package com.job.controller;

import com.job.model.JobResult;
import com.job.processor.JobProcessor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@Slf4j
@RestController
public class JobPreProcessorResource {

    @Autowired
    private JobProcessor jobProcessor;

    public JobPreProcessorResource() {
        log.info("JobPreProcessorResource started");
    }

    @PostMapping(path = "job")
    public ResponseEntity<JobResult> postJob(@Valid @RequestBody JobResult job) {
        jobProcessor.scheduleJob(job);
        return ResponseEntity.status(HttpStatus.CREATED).body(job);
    }

    @PostMapping(path = "jobs")
    public ResponseEntity<JobResult> postJob(@Valid @RequestBody List<JobResult> jobs) {
        jobs.stream().forEach(job -> jobProcessor.scheduleJob(job));
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

}
