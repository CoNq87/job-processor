package com.job.processor;

import com.job.model.Job;
import com.job.model.JobResult;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
public class MultiQueuePriorityJobProcessorTest extends BaseTest {

    @Test
    public void testBachScheduleJob() {


        MultiQueuePriorityJobProcessor multiQueuePriorityJobProcessor = new MultiQueuePriorityJobProcessor(
                1, 30, 2, 30, 2, 30
        );

        MultiQueuePriorityJobProcessor mock = Mockito.spy(multiQueuePriorityJobProcessor);

        List<Job> jobs = getTestJobs();

        for (Job job : jobs) {
            mock.scheduleJob(job);
        }

        ArgumentCaptor<JobResult> jobCaptor = ArgumentCaptor.forClass(JobResult.class);
        Mockito.verify(mock, Mockito.times(14)).scheduleJob(jobCaptor.capture());

    }

    @Ignore("manual test")
    @Test
    public void manualTestBachScheduleJob() throws Exception {
        MultiQueuePriorityJobProcessor multiQueuePriorityJobProcessor = new MultiQueuePriorityJobProcessor(
                1, 30, 2, 30, 2, 30
        );

        List<Job> jobs = getTestJobs();

        for (Job job : jobs) {
            multiQueuePriorityJobProcessor.scheduleJob(job);
        }


        Thread.sleep(20000);
    }
}
