package com.job.processor;

import com.job.model.Job;
import com.job.model.JobResult;
import com.job.model.Priority;

import java.util.LinkedList;
import java.util.List;

public abstract class BaseTest {

    protected List<Job> getTestJobs() {
        List<Job> jobs = new LinkedList<>();
        for (int i = 0; i < 10; i++) {
            jobs.add(new JobResult(Priority.LOW));
        }
        for (int i = 0; i < 3; i++) {
            jobs.add(new JobResult(Priority.MEDIUM));
        }
        for (int i = 0; i < 1; i++) {
            jobs.add(new JobResult(Priority.HIGH));
        }
        return jobs;
    }
}
