package com.job.processor;


import com.job.model.Job;
import com.job.model.JobResult;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
public class SimplePriorityJobProcessorTest extends BaseTest {

    @Test
    public void testBachScheduleJob() {


        SimplePriorityJobProcessor simplePriorityJobProcessor = new SimplePriorityJobProcessor(
                1, 30
        );

        SimplePriorityJobProcessor mock = Mockito.spy(simplePriorityJobProcessor);

        List<Job> jobs = getTestJobs();

        for (Job job : jobs) {
            mock.scheduleJob((JobResult) job);
        }

        ArgumentCaptor<JobResult> jobCaptor = ArgumentCaptor.forClass(JobResult.class);
        Mockito.verify(mock, Mockito.times(14)).scheduleJob(jobCaptor.capture());

    }

    @Ignore("manual test")
    @Test
    public void manualTestBachScheduleJob() throws Exception {
        SimplePriorityJobProcessor simplePriorityJobProcessor = new SimplePriorityJobProcessor(
                1, 30
        );

        List<Job> jobs = getTestJobs();

        for (Job job : jobs) {
            simplePriorityJobProcessor.scheduleJob((JobResult) job);
        }


        Thread.sleep(20000);
    }
}
