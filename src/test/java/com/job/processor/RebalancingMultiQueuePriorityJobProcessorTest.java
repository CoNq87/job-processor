package com.job.processor;

import com.job.model.Job;
import com.job.model.JobResult;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
public class RebalancingMultiQueuePriorityJobProcessorTest extends BaseTest {

    @Test
    public void testBachScheduleJob() {


        RebalancingMultiQueuePriorityJobProcessor rebalancingMultiQueuePriorityJobProcessor = new RebalancingMultiQueuePriorityJobProcessor(
                1, 30, 2, 30, 2, 30, 4L
        );

        RebalancingMultiQueuePriorityJobProcessor mock = Mockito.spy(rebalancingMultiQueuePriorityJobProcessor);

        List<Job> jobs = getTestJobs();

        for (Job job : jobs) {
            mock.scheduleJob(job);
        }

        ArgumentCaptor<JobResult> jobCaptor = ArgumentCaptor.forClass(JobResult.class);
        Mockito.verify(mock, Mockito.times(14)).scheduleJob(jobCaptor.capture());

    }

    @Ignore("manual test")
    @Test
    public void manualTestBachScheduleJob() throws Exception {
        RebalancingMultiQueuePriorityJobProcessor rebalancingMultiQueuePriorityJobProcessor = new RebalancingMultiQueuePriorityJobProcessor(
                1, 30, 2, 30, 2, 30, 4L
        );

        List<Job> jobs = getTestJobs();

        for (Job job : jobs) {
            rebalancingMultiQueuePriorityJobProcessor.scheduleJob(job);
        }


        Thread.sleep(20000);
    }


}
